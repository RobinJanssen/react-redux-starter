'use strict'

const Generator = require("yeoman-generator")
const yeoman = require("yeoman-environment")

const env = yeoman.createEnv()

module.exports = class extends Generator {

	constructor(args,opts){
		super(args,opts)

		this.argument('appname', { type: String, required: false });
		this.option('babel')
		this.answers = {}
	}

	prompting(){
		return this.prompt([
			{
				type    : 'input',
				name    : 'name',
				message : 'Your project name',
				default : this.options.appname
			}, 
			{
				type    : 'input',
				name    : 'author',
				message : 'Your name',
				default : 'Robin Janssen'
			},
			{
				type    : 'input',
				name    : 'description',
				message : 'App description',
				default : ''
			},
			{
				type    : 'confirm',
				name    : 'useSemantic',
				message : 'Would you like to use Semantic UI?'
			}
		]).then((answers) => {
			this.answers = answers
			this.answers.appname = this.answers.name.toLowerCase().replace(' ','-')

			if(this.answers.useSemantic)
				return this.prompt([{
					type    : 'confirm',
					name    : 'semanticcdn',
					message : 'Load Semantic UI from CDN?'
				}]).then(a=>{
					this.answers.semanticCdn = a.semanticcdn
				})
			return
			
	    })
	}

	copyConfig(){
		const confs = ['webpack.config.js','.babelrc','.bowerrc','.editorconfig','.eslintrc.json','.gitignore','package.json','bower.json','server.js']

		confs.forEach(conf=>{
			this.fs.copyTpl(
				this.templatePath(conf),
				this.destinationPath(conf),
				this.answers
			)
		})
	}
	copyWebapp(){
		this.fs.copyTpl(
			this.templatePath('./src'),
			this.destinationPath('./src'),
			this.answers
		)
	}

	runInstalls(){
		this.npmInstall()

		if(this.answers.useSemantic && !this.answers.semanticCdn)
			this.bowerInstall(['semantic-ui'], { 'save': true });

		this.bowerInstall()
	}

}