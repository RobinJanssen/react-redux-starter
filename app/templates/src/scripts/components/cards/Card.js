import React from 'react'

const Card = ({item}) => (
  <div className="ui card">
    <div className="image">
      <img src={`/images/${item.main_image}`}/>
    </div>
    <div className="content">
      <a href="" className="header">{item.title}</a>
      <div className="meta">
        <span className="">Up votes: {item.votes.up_votes}</span>
        <span className="right floated">Down votes: {item.votes.down_votes}</span>
      </div>
    </div>
    <div className="extra content">
      <div className="ui two buttons">
        <div className="ui basic green button">Edit</div>
        <div className="ui basic red button">Delete</div>
      </div>
    </div>
  </div>
)

export {Card}