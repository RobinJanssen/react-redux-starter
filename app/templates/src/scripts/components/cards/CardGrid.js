import React from 'react'

import {Card} from './Card'

const CardGrid = ({items}) => (
  <div className="ui cards grid">
    {items.map(item=>(
      <Card key={item._id} item={item}/>
    ))}
  </div>
)

export {CardGrid}