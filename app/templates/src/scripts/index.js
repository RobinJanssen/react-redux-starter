import 'babel-polyfill'
import React from 'react'
import ReactDOM from 'react-dom'
import thunkMiddleware from 'redux-thunk'

import { Provider } from 'react-redux'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { Router, Route, browserHistory, IndexRoute } from 'react-router'
import { syncHistoryWithStore, routerReducer } from 'react-router-redux'

import configureStore from './store'

const store = configureStore()
// Create an enhanced history that syncs navigation events with the store
const history = syncHistoryWithStore(browserHistory, store)

import {App} from './App'

//TODO: Replace this with your own 
import { DefaultPageContainer } from './modules/default'

ReactDOM.render(
    <Provider store={store}>
      <Router history={history}>
        <Route path="/" component={App}>
        //TODO: Replace with your own routes
        <IndexRoute component={DefaultPageContainer} />

        </Route>
      </Router>
    </Provider>,
    document.getElementById('root')
)


