import { combineReducers } from 'redux'
import {routerReducer as routing} from 'react-router-redux'

const allReducers = {} //Make sure to import all reducers

const rootReducer = combineReducers({
  ...allReducers,
  routing
})

export default rootReducer