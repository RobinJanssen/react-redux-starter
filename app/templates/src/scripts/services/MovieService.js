import fetch from 'isomorphic-fetch'

const MovieServiceFactory = () => (
  Object.create({
    //Get all movies
    getAll(){
      return fetch('http://localhost:8001/api/movies')
    },
    //Get one movie by id
    getOne(id){
      return fetch(`http://localhost:8001/api/movies/${id}`)
    }
  })
)

export default MovieServiceFactory
