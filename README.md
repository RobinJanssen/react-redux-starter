
#React Redux Webpack starter


##Prerequisites

The generator assumes that you have the following software on your system.

* NodeJS + npm + Yeoman (the usual dependencies for running Yeoman)

* Bower

The project that is generated with this starter depends on the following global npm packages:

* webpack

* webpack-dev-server


##Installing the generator

Installing this Yeoman Generator pretty straightforward. 

* git clone this repository into a dedicated folder for your generator

* cd into the directory of the repository

* run `npm link` (use sudo if necessary)

From now on this generator is available 


##Using the generator

* create a new folder for your project

* run `yo reactredux`

* follow the steps in the wizard